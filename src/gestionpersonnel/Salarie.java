
package gestionpersonnel;

import         java.util.Date;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.convDateVersChaine;

public class Salarie {
    
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private Long   id;
    private String nom;
    private String prenom;
    private String sexe;
    private Date   dateNaiss;
    private Float  salaire;
    
    // Attribut navigationnel
    private Pole lePole;
   
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Salarie() { }
    
    public Salarie(Long id, String nom, String prenom, String sexe, Date dateNaiss, Float salaire) {
        
        this.id        = id;
        this.nom       = nom;
        this.prenom    = prenom;
        this.sexe      = sexe;
        this.dateNaiss = dateNaiss;
        this.salaire   = salaire;
    }
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Méthodes">
    
    public void afficher(){
        
       System.out.printf(
                
                " %2d %-12s %-12s %-2s %-8s %2d ans %8.2f €",
                this.id,
                this.nom,
                this.prenom,
                this.sexe,
                convDateVersChaine(this.dateNaiss),
                ageEnAnnees(this.dateNaiss),
                this.salaire
        );
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
      
    public Long getId() {
        return id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
   
    public String getPrenom() {
        return prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public Float getSalaire() {
        return salaire;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public void setSalaire(Float salaire) {
        this.salaire = salaire;
    }

    public Pole getLePole() {
        return lePole;
    }

    public void setLePole(Pole lePole) {
        this.lePole = lePole;
    }
    
      //</editor-fold>

}




