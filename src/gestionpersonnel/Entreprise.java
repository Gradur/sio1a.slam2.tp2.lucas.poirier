
package gestionpersonnel;

import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.convChaineVersDate;

public class Entreprise {
    
   //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private static List<Salarie>   tousLesSalaries = new LinkedList();
    private static List<Pole>      tousLesPoles    = new LinkedList();
     
    //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="Code Statique">
    
    static{
      
      Pole  p1= new Pole("SLAM","Solutions Logicielles et Applications Métiers");
      Pole  p2= new Pole("SISR","Solutions d'Infrastructures Systèmes et Réseaux");  
        
      tousLesPoles.add(p1);tousLesPoles.add(p2);
      
      Salarie s1 = new Salarie(101L, "Durant", "Pierre", "M", convChaineVersDate("12/05/1983"), 1830f );   
      tousLesSalaries.add( s1);affecter(s1, p1);
      
      Salarie s2 = new Salarie(102L,"Martin", "Pierre", "M", convChaineVersDate("25/11/1991"), 2100f ); 
      tousLesSalaries.add( s2);affecter(s2, p1);
      
      Salarie s3 = new Salarie(103L,"Lecoutre", "Thierry", "M", convChaineVersDate("05/08/1989"), 2320f ); 
      tousLesSalaries.add( s3);affecter(s3,p2);
      
      Salarie s4 = new Salarie(104L,"Duchemin", "Fabienne", "F", convChaineVersDate("14/03/1992"), 1788f ); 
      tousLesSalaries.add( s4);affecter(s4,p1);
       
      Salarie s5=  new Salarie(105L,"Duchateau", "Jacques", "M", convChaineVersDate("18/07/1992"), 1991f ) ;
      tousLesSalaries.add( s5);affecter(s5,p2);
      
      Salarie s6= new Salarie(106L,"Lemortier", "Laurent", "M", convChaineVersDate("18/02/1980"), 1878f ) ;
      tousLesSalaries.add( s6);affecter(s6,p1);
       
      Salarie s7=  new Salarie(107L,"Dessailles", "Sabine", "F", convChaineVersDate("23/03/1990"), 2450f );
      tousLesSalaries.add( s7);affecter(s7,p1);
     
      Salarie s8=new Salarie(108L,"Bataille", "Boris", "M", convChaineVersDate("17/11/1987"), 1802f );
      tousLesSalaries.add( s8);affecter(s8,p2);
      
      Salarie s9=new Salarie(109L,"Lerouge", "Laëtitia", "F", convChaineVersDate("09/10/1992"), 1946f ); 
      tousLesSalaries.add( s9);affecter(s9,p2);
       
      Salarie s10=new Salarie(110L,"Renard", "Paul ", "M", convChaineVersDate("16/08/1985"), 2145f ); 
      tousLesSalaries.add( s10);affecter(s10,p1);
     
      Salarie s11=  new Salarie(111L,"Durant", "Jacques", "M", convChaineVersDate("13/04/1990"), 1755f ); 
      tousLesSalaries.add( s11);affecter(s11,p2);
     
      Salarie s12= new Salarie(112L,"Delespaul" , "Martine" , "F", convChaineVersDate("25/02/1991"), 2015f );
      tousLesSalaries.add( s12);affecter(s12,p1);
       
    }
    
    //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="Méthodes Métiers">
    
    public static Salarie      getSalarieDeNumero(Long pId){
        
        Salarie salarie=null;
        
        for(Salarie unSalarie: tousLesSalaries){
            
            if( unSalarie.getId()==pId){ 
                salarie=unSalarie;break;
            }
        }
        
        return salarie;
    }
     
    public static Pole         getPoleDeCode(String pCodePole){
        
        Pole pole=null;
        
        for(Pole unPole: tousLesPoles){
            
         if( unPole.getCodePole().equals(pCodePole)){
                pole=unPole;break;
         }
        }
        
        return pole;
    }
   
    public static void         affecter(Salarie s, Pole p) {
        
        p.getLesSalaries().add(s);s.setLePole(p);
    }
    
    //</editor-fold>

   //<editor-fold defaultstate="collapsed" desc="Getters">
    
    public static List<Salarie> getTousLesSalaries() {
        return tousLesSalaries;
    }

    public static List<Pole> getTousLesPoles() {
        return tousLesPoles;
    }
    
    //</editor-fold>

}


