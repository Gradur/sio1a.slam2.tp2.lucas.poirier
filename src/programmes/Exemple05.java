package programmes;

import        gestionpersonnel.Salarie;
import static gestionpersonnel.Entreprise.getSalarieDeNumero;
import        java.util.Scanner;

public class Exemple05 {

    Scanner scan= new Scanner(System.in);
           
    public  void executer() {
       
       System.out.printf("\nRecherche d'un salarié par son numéro\n\n");
      
       System.out.print("Saisir l'identifiant du salarie: ");
       Long idSal=scan.nextLong();
       
       Salarie salarie= getSalarieDeNumero(idSal);
       
       if ( salarie!=null ){
       
          System.out.println();
          salarie.afficher();
       
          String debutPhrase=salarie.getSexe().equals("M")? "Il est affecté":"Elle est affectée";
       
          System.out.printf("\n\n "+ debutPhrase+" au Pôle %-40s\n\n",salarie.getLePole().getNomPole());
       }       
       else System.out.printf("\n Il n'a pas de salarié de numéro %2d\n",idSal);
       
 }
}

