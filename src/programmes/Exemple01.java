package programmes;

import        gestionpersonnel.Salarie;
import static gestionpersonnel.Tris.trierParNomPrenom;
import static gestionpersonnel.Entreprise.getTousLesSalaries;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple01 {

    private final String formatTitre="\nListe de salariés à la date du: %-8s \n\n"; 
    
    public  void executer() {
        
        System.out.printf(formatTitre, aujourdhuiChaine());
      
        for( Salarie sal:  trierParNomPrenom( getTousLesSalaries() ) ){
        
              System.out.printf(" %3d %-15s\n", sal.getId(),sal.getNom()); 
        }
        
        System.out.println();
    }
}

