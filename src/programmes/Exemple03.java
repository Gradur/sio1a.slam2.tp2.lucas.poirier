package programmes;

import        gestionpersonnel.Salarie;
import static gestionpersonnel.Entreprise.getTousLesSalaries;
import static gestionpersonnel.Tris.trierParNomPrenom;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple03 {

    private final String formatTitre="\nListe de salariés à la date du: %-8s \n\n"; 
    
    public  void executer() {
     
       System.out.printf(formatTitre, aujourdhuiChaine());
      
       for( Salarie sal: trierParNomPrenom( getTousLesSalaries() ) ){
        
              sal.afficher();
              System.out.println();
        
       }
        
       System.out.println();
        
       
    }
}

